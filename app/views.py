from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from . forms  import MyUserForm
from . models import MyUser, Profile

# Create your views here.

class IndexView(TemplateView):
    template_name = "index.html"

class SuccessView(TemplateView):
    template_name = "success.html"

class SignUpView(TemplateView):
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        form = MyUserForm()
        return render(request, self.template_name, {'form': form})
    
    def post(self, request, *args, **kwargs):
        form = MyUserForm(request.POST)
        if form.is_valid():
            user = MyUser.objects.create_user(
                    request.POST.get('username'),
                    request.POST.get('email'),
                    request.POST.get('password')
                )
            user.first_name = request.POST.get('first_name')
            user.last_name = request.POST.get('last_name')
            user.phone_number = request.POST.get('phone_number')
            user.save()
            return redirect('registro/success')
        return render(request, self.template_name, {'form': form})