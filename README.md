# Singup App
### Stack: Django + Django Rest Framework + SQLITE DB
### Author: Carlos sarmiento.
##

Run App: 
``` 
$ py manage.py runserver
```

Stop App: 
``` 
$ ctrl + c
```

Show the app running in URL: http://localhost:8000